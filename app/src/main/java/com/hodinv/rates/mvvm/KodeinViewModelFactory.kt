@file:Suppress("UNCHECKED_CAST")

package com.hodinv.rates.mvvm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import org.kodein.di.generic.instance

inline fun <reified ViewModelT : BaseViewModel> BaseMvvmFragment<ViewModelT>.viewModelLazyInstance() =
    lazy {
        this.activity?.let { activity ->
            ViewModelProviders
                .of(activity, object : ViewModelProvider.Factory {
                    override fun <T : ViewModel> create(modelClass: Class<T>): T {
                        return this@viewModelLazyInstance.kodein.run {
                            val viewModel by instance<ViewModelT>()
                            viewModel
                        } as T
                    }
                })
                .get(ViewModelT::class.java)
        } ?: ViewModelProviders
            .of(this, object : ViewModelProvider.Factory {
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return this@viewModelLazyInstance.kodein.run {
                        val viewModel by instance<ViewModelT>()
                        viewModel
                    } as T
                }
            })
            .get(ViewModelT::class.java)
    }


