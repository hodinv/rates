package com.hodinv.rates.mvvm

import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

abstract class BaseMvvmFragment<VM : BaseViewModel> : Fragment(), KodeinAware {

    override val kodein: Kodein by kodein()

    abstract val viewModel: VM

    protected val disposables = CompositeDisposable()
}