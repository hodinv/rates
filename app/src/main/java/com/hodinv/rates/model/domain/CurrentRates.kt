package com.hodinv.rates.model.domain

data class CurrentRates(
    val code: String,
    val rate: Double
)