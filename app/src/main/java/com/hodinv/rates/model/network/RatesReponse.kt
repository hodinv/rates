package com.hodinv.rates.model.network

class RatesReponse(
    val baseCurrency: String,
    val rates: Map<String, Double>
)