package com.hodinv.rates.model.domain

data class CurrencyItem(
    val code: String,
    var value: Double,
    val isTop: Boolean,
    val title: String = "",
    val iconUrl: String = ""
)