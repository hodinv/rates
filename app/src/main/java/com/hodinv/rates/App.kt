package com.hodinv.rates

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.hodinv.rates.interactors.*
import com.hodinv.rates.screens.rateslist.RatesListViewModel
import com.hodinv.rates.services.network.NetworkProvider
import com.hodinv.rates.services.network.NetworkProviderImpl
import com.hodinv.rates.services.network.api.RatesApi
import com.hodinv.rates.utils.CurrencyInfoProvider
import com.hodinv.rates.utils.CurrencyInfoProviderImpl
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

@Suppress("unused")
class App : Application(), KodeinAware {

    override val kodein by lazy {
        Kodein {
            bind<Context>() with singleton { this@App }
            bind<Gson>() with singleton { Gson() }

            bind<RatesListViewModel>() with provider { RatesListViewModel(instance(), instance()) }

            bind<GetRatesInteractor>() with singleton { GetRatesInteractorImpl(instance()) }
            bind<CalculateRatesInteractor>() with singleton { CalculateRatesInteractorImpl(instance()) }


            bind<NetworkProvider>() with singleton { NetworkProviderImpl(getString(R.string.base_url)) }
            bind<RatesApi>() with singleton { instance<NetworkProvider>().ratesApi }

            bind<CalculateMetaInfoInteractor>() with singleton {
                CalculateMetaInfoInteractorImpl(
                    instance()
                )
            }
            bind<CurrencyInfoProvider>() with singleton { CurrencyInfoProviderImpl(instance()) }

        }
    }

    override fun onCreate() {
        super.onCreate()
        kodein.run {
        }
    }
}