package com.hodinv.rates.interactors

import com.hodinv.rates.model.domain.CurrencyItem
import io.reactivex.Observable

interface CalculateRatesInteractor {
    fun calculateRates(
        topItem: Observable<Pair<String, Double>>
    ): Observable<List<CurrencyItem>>
}