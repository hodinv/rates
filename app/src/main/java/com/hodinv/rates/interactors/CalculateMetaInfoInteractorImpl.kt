package com.hodinv.rates.interactors

import com.hodinv.rates.model.domain.CurrencyItem
import com.hodinv.rates.utils.CurrencyInfoProvider

class CalculateMetaInfoInteractorImpl(private val provider: CurrencyInfoProvider) :
    CalculateMetaInfoInteractor {
    override fun wrapWithInfo(list: List<CurrencyItem>): List<CurrencyItem> {
        return list.map {
            it.copy(iconUrl = provider.getIcon(it.code), title = provider.getName(it.code))
        }
    }

}