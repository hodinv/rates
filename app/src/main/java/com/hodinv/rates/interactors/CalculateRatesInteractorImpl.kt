package com.hodinv.rates.interactors

import com.hodinv.rates.model.domain.CurrencyItem
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers

class CalculateRatesInteractorImpl(private val ratesInteractor: GetRatesInteractor) :
    CalculateRatesInteractor {
    override fun calculateRates(
        topItem: Observable<Pair<String, Double>>
    ): Observable<List<CurrencyItem>> {
        return ratesInteractor.getRates().firstOrError().flatMapObservable {
            val first = it[0]
            Observables.combineLatest(
                Observable.concat(Observable.just(Pair(first.code, 1.0)), topItem).distinctUntilChanged(),
                ratesInteractor.getRates()
            ) { (top, topValue), rates ->
                val topRate = rates.first { it.code == top }.rate
                rates.sortedBy {
                    if (it.code == top) "AAA" else it.code
                }.map {
                    CurrencyItem(
                        it.code,
                        (topValue / topRate) * it.rate,
                        it.code == top
                    )
                }
            }
        }.subscribeOn(Schedulers.io())
    }
}
