package com.hodinv.rates.interactors

import com.hodinv.rates.model.domain.CurrentRates
import io.reactivex.Observable

interface GetRatesInteractor {
    fun getRates(): Observable<List<CurrentRates>>
}