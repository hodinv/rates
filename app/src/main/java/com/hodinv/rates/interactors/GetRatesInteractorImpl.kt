package com.hodinv.rates.interactors


import com.hodinv.rates.model.domain.CurrentRates
import com.hodinv.rates.services.network.api.RatesApi
import io.reactivex.BackpressureOverflowStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class GetRatesInteractorImpl(private val api: RatesApi) : GetRatesInteractor {
    override fun getRates(): Observable<List<CurrentRates>> {
        return Flowable.interval(0, 1, TimeUnit.SECONDS).onBackpressureBuffer(
            1,
            {},
            BackpressureOverflowStrategy.DROP_OLDEST
        ).flatMapSingle({
            api.getRates().retryWhen { throwable -> throwable.delay(1, TimeUnit.SECONDS) }
        }, false, 1)
            .toObservable().map {
                listOf(CurrentRates(it.baseCurrency, 1.0)).plus(it.rates.map {
                    CurrentRates(
                        it.key,
                        it.value
                    )
                })
            }.subscribeOn(Schedulers.io())
    }
}

