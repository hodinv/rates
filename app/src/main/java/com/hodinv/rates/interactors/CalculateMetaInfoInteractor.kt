package com.hodinv.rates.interactors

import com.hodinv.rates.model.domain.CurrencyItem

interface CalculateMetaInfoInteractor {
    fun wrapWithInfo(list: List<CurrencyItem>): List<CurrencyItem>
}