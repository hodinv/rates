package com.hodinv.rates.utils

interface CurrencyInfoProvider{
    fun getIcon(currency: String): String
    fun getName(currency: String): String
}