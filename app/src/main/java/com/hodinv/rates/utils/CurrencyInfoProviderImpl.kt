package com.hodinv.rates.utils

import android.content.Context
import com.hodinv.rates.R

class CurrencyInfoProviderImpl(val context: Context) : CurrencyInfoProvider {
    override fun getIcon(currency: String): String {
        return when (currency.toUpperCase()) {
            "USD" -> "https://www.countryflags.io/us/flat/64.png"
            "EUR" -> "https://www.countryflags.io/eu/flat/64.png"
            "AUD" -> "https://www.countryflags.io/au/flat/64.png"
            "BGN" -> "https://www.countryflags.io/bg/flat/64.png"
            "BRL" -> "https://www.countryflags.io/br/flat/64.png"
            "CAD" -> "https://www.countryflags.io/ca/flat/64.png"
            "CHF" -> "https://www.countryflags.io/ch/flat/64.png"
            "CNY" -> "https://www.countryflags.io/cn/flat/64.png"
            "CZK" -> "https://www.countryflags.io/cz/flat/64.png"
            "DKK" -> "https://www.countryflags.io/dk/flat/64.png"
            "GBP" -> "https://www.countryflags.io/gb/flat/64.png"
            "HKD" -> "https://www.countryflags.io/hk/flat/64.png"
            "HRK" -> "https://www.countryflags.io/hr/flat/64.png"
            "HUF" -> "https://www.countryflags.io/hu/flat/64.png"
            "IDR" -> "https://www.countryflags.io/id/flat/64.png"
            "ILS" -> "https://www.countryflags.io/il/flat/64.png"
            "INR" -> "https://www.countryflags.io/in/flat/64.png"
            "ISK" -> "https://www.countryflags.io/is/flat/64.png"
            "JPY" -> "https://www.countryflags.io/jp/flat/64.png"
            "KRW" -> "https://www.countryflags.io/kr/flat/64.png"
            "MXN" -> "https://www.countryflags.io/mx/flat/64.png"
            "MYR" -> "https://www.countryflags.io/my/flat/64.png"
            "NOK" -> "https://www.countryflags.io/no/flat/64.png"
            "NZD" -> "https://www.countryflags.io/nz/flat/64.png"
            "PHP" -> "https://www.countryflags.io/ph/flat/64.png"
            "PLN" -> "https://www.countryflags.io/pl/flat/64.png"
            "RON" -> "https://www.countryflags.io/ro/flat/64.png"
            "RUB" -> "https://www.countryflags.io/ru/flat/64.png"
            "SEK" -> "https://www.countryflags.io/se/flat/64.png"
            "SGD" -> "https://www.countryflags.io/sg/flat/64.png"
            "THB" -> "https://www.countryflags.io/th/flat/64.png"
            "ZAR" -> "https://www.countryflags.io/za/flat/64.png"
            else -> ""
        }
    }

    override fun getName(currency: String): String {
        return when (currency.toUpperCase()) {
            "USD" -> context.getString(R.string.currency_usd)
            "EUR" -> context.getString(R.string.currency_eur)
            "AUD" -> context.getString(R.string.currency_aud)
            "BGN" -> context.getString(R.string.currency_bgn)
            "BRL" -> context.getString(R.string.currency_brl)
            "CAD" -> context.getString(R.string.currency_cad)
            "CHF" -> context.getString(R.string.currency_chf)
            "CNY" -> context.getString(R.string.currency_cny)
            "CZK" -> context.getString(R.string.currency_czk)
            "DKK" -> context.getString(R.string.currency_dkk)
            "GBP" -> context.getString(R.string.currency_gbp)
            "HKD" -> context.getString(R.string.currency_hkd)
            "HRK" -> context.getString(R.string.currency_hrk)
            "HUF" -> context.getString(R.string.currency_huf)
            "IDR" -> context.getString(R.string.currency_idr)
            "ILS" -> context.getString(R.string.currency_ils)
            "INR" -> context.getString(R.string.currency_inr)
            "ISK" -> context.getString(R.string.currency_isk)
            "JPY" -> context.getString(R.string.currency_jpy)
            "KRW" -> context.getString(R.string.currency_krw)
            "MXN" -> context.getString(R.string.currency_mxn)
            "MYR" -> context.getString(R.string.currency_myr)
            "NOK" -> context.getString(R.string.currency_nok)
            "NZD" -> context.getString(R.string.currency_nzd)
            "PHP" -> context.getString(R.string.currency_php)
            "PLN" -> context.getString(R.string.currency_pln)
            "RON" -> context.getString(R.string.currency_ron)
            "RUB" -> context.getString(R.string.currency_rub)
            "SEK" -> context.getString(R.string.currency_sek)
            "SGD" -> context.getString(R.string.currency_sgd)
            "THB" -> context.getString(R.string.currency_thb)
            "ZAR" -> context.getString(R.string.currency_zar)
            else -> currency
        }
    }

}