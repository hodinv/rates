package com.hodinv.rates.services.network.api

import com.hodinv.rates.model.network.RatesReponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {
    @GET("latest")
    fun getRates(@Query("base") baseCurrency : String = "EUR"): Single<RatesReponse>
}