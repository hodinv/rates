package com.hodinv.rates.services.network

import com.hodinv.rates.services.network.api.RatesApi

interface NetworkProvider {
    val ratesApi: RatesApi
}