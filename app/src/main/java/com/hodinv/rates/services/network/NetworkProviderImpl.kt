package com.hodinv.rates.services.network

import com.hodinv.rates.BuildConfig
import com.hodinv.rates.services.network.api.RatesApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NetworkProviderImpl(baseUrl: String) : NetworkProvider {

    private val api: Retrofit

    init {
        val httpClient = OkHttpClient.Builder()

        addLogging(httpClient)

        val client = httpClient.build()

        api = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
    }

    override val ratesApi: RatesApi = api.create(RatesApi::class.java)


    private fun addLogging(httpClient: OkHttpClient.Builder) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        httpClient.addInterceptor(loggingInterceptor)
    }
}