package com.hodinv.rates.screens.rateslist.adapter

interface ItemCallback {
    fun onValue(code: String, double: Double)
}