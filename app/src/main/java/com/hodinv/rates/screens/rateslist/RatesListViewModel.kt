package com.hodinv.rates.screens.rateslist

import android.util.Log
import androidx.databinding.ObservableField
import com.hodinv.rates.interactors.CalculateMetaInfoInteractor
import com.hodinv.rates.interactors.CalculateRatesInteractor
import com.hodinv.rates.model.domain.CurrencyItem
import com.hodinv.rates.mvvm.BaseViewModel
import com.hodinv.rates.screens.rateslist.adapter.ItemCallback
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RatesListViewModel(
    calculateRatesInteractor: CalculateRatesInteractor,
    private val infoProvider: CalculateMetaInfoInteractor
) : BaseViewModel(),
    ItemCallback {

    override fun onValue(code: String, double: Double) {
        topItem.accept(Pair(code, double))
    }

    val items = ObservableField<List<CurrencyItem>>(emptyList())
    private val topItem = PublishRelay.create<Pair<String, Double>>()
    val empty = ObservableField<Boolean>(true)


    init {
        addDisposable(
            calculateRatesInteractor.calculateRates(
                topItem.subscribeOn(Schedulers.io())
            ).observeOn(AndroidSchedulers.mainThread()).subscribe { it ->
                val list = ArrayList(it)
                val prevList = items.get()
                if (prevList != null && prevList.isNotEmpty() && list.isNotEmpty() && list[0].code == prevList[0].code) {
                    prevList.forEach { oldItem ->
                        list.firstOrNull { it.code == oldItem.code }?.also { newItem ->
                            oldItem.value = newItem.value
                        }
                    }
                    items.set(ArrayList(prevList))
                } else {
                    items.set(infoProvider.wrapWithInfo(list))
                    empty.set(list.isEmpty())
                }
            }
        )
    }


    override fun onCleared() {
        super.onCleared()
        Log.d("MYVM", "cleared")
    }
}