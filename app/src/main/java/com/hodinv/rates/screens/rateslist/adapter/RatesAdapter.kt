package com.hodinv.rates.screens.rateslist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.hodinv.rates.R
import com.hodinv.rates.model.domain.CurrencyItem

class RatesAdapter(private val itemCallback: ItemCallback) : ListAdapter<CurrencyItem, CurrencyViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        return CurrencyViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.listitem_currency,
                parent,
                false
            ), itemCallback
        )
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CurrencyItem>() {
            override fun areItemsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
                return oldItem.code == newItem.code
            }

            override fun areContentsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
                return  newItem.isTop && oldItem.isTop
            }

            override fun getChangePayload(oldItem: CurrencyItem, newItem: CurrencyItem): Any? {
                return if(newItem.isTop != oldItem.isTop) super.getChangePayload(oldItem, newItem) else Unit
            }
        }
    }
}