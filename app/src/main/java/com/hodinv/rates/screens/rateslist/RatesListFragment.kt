package com.hodinv.rates.screens.rateslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.hodinv.rates.R
import com.hodinv.rates.databinding.FragmentRateslistBinding
import com.hodinv.rates.mvvm.BaseMvvmFragment
import com.hodinv.rates.mvvm.viewModelLazyInstance
import com.hodinv.rates.screens.rateslist.adapter.RatesAdapter

class RatesListFragment : BaseMvvmFragment<RatesListViewModel>() {
    override val viewModel: RatesListViewModel by viewModelLazyInstance()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentRateslistBinding>(
            inflater,
            R.layout.fragment_rateslist,
            container,
            false
        )
        binding.viewModel = viewModel
        binding.adapter = RatesAdapter(viewModel)
        return binding.root
    }
}