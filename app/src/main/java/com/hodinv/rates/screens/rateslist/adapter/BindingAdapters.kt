package com.hodinv.rates.screens.rateslist.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

object BindingAdapters {

    @BindingAdapter(value = ["app:items", "app:adapter"], requireAll = true)
    @JvmStatic
    fun setItems(view: RecyclerView, items: List<Any>, adapter: RecyclerView.Adapter<*>) {
        if (view.adapter != adapter) {
            view.adapter = adapter
        }
        view.adapter?.also {
            (it as ListAdapter<Any, *>).submitList(items)
        }
    }
}