package com.hodinv.rates.screens.rateslist.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.hodinv.rates.model.domain.CurrencyItem
import com.hodinv.rates.utils.GlideApp
import kotlinx.android.synthetic.main.listitem_currency.view.*
import java.text.DecimalFormat

class CurrencyViewHolder(private val view: View, private val itemCallback: ItemCallback) :
    RecyclerView.ViewHolder(view) {

    var currentItem: CurrencyItem? = null

    fun sendValue() {
        currentItem?.also { item ->
            itemCallback.onValue(
                item.code,
                view.amount.text.toString().toDoubleOrNull() ?: 0.0
            )
        }
    }

    private val watcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            if (currentItem?.isTop == true) {
                sendValue()
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    }

    init {
        view.setOnClickListener {
            view.amount.requestFocus()
        }
        view.amount.setOnFocusChangeListener { _, focused ->
            if (focused) {
                currentItem?.also { item ->
                    if (!item.isTop) sendValue()
                }

            }
        }
        view.amount.addTextChangedListener(watcher)
    }


    fun bind(item: CurrencyItem) {
        currentItem = item
        view.currency.text = item.code
        view.currencyInfo.text = item.title
        GlideApp.with(view.context)
            .load(item.iconUrl)
            .centerCrop()
            .into(view.icon)
        val amount = String.format("%.2f", item.value).replace(separator, ".")
        if (view.amount.text.toString() != amount) {
            view.amount.setText(amount)
        }
    }

    companion object {
        val separator: String = let {
            (DecimalFormat.getInstance() as? DecimalFormat)?.decimalFormatSymbols?.decimalSeparator?.toString()
                ?: ","
        }
    }
}